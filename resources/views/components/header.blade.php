<div class="row pt-3 pb-3 Header">
    <div class="col-sm-3 col-xs-12 Header__logo">
        <a href="/">
            <img src="/images/nasa_logo.png" alt="Nasa image and video library"/>
        </a>
    </div>
    <div class="col-sm-9 col-xs-12 Header__form">
        <div id="app">
            <search-form action="{{route('search')}}" query="{{ $query ?? '' }}"></search-form>
        </div>
    </div>
</div>
