@extends('layout')

@section('title')
    Nasa image and video library - {{ $asset->getTitle() }}
@endsection

@section('content')
    <div class="row page-block asset">
        <div class="col-sm-6 col-xs-12 mb-3">
            @yield('media')
        </div>
        <div class="col-sm-6 col-xs-12 mb-3 asset__details">
            <h1>{{ $asset->getTitle() }}</h1>
            <span class="asset__label">Keywords:</span> {{ implode(', ', $asset->getKeywords()) }} <br>
            <span class="asset__label">Center:</span> {{ $asset->getCenter() }}<br>
            <span class="asset__label">Date Created:</span> {{ $asset->getDateCreated() ? $asset->getDateCreated()->format('d/m/Y') : '' }}<br>
            <button class="button mt-2" onclick="window.history.go(-1);">Go Back</button>
        </div>
    </div>
@endsection
