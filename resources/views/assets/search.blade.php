@extends('layout')

@section('title')
    Nasa image and video library
@endsection

@section('content')
    @if(!empty($items))
        <div class="row page-block">
            @foreach($items as $indexKey => $item)
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 mb-2">
                    <a class="asset-preview" href="{{ route('asset_details', ['assetId' => $item->getAssetId()]) }}">
                        <div class="asset-preview__cover">
                            @if($item->getType() === 'image'))
                                <img src="{{ $item->getThumbnailUrl() }}" alt="{{ $item->getTitle() }}"/>
                            @elseif($item->getType() === 'audio')
                                <i class="fas fa-headphones-alt"></i>
                            @elseif($item->getType() === 'video')
                                <i class="fas fa-video"></i>
                            @endif
                        </div>
                        <div class="asset-preview__title"><div>{{ $item->getTitle() }}</div></div>
                    </a>
                </div>
            @endforeach
        </div>
    @elseif(!empty($query))
        <div class="Message Message--error">Sorry no items matched your search for "{{ $query }}".</div>
    @else
        <div class="Message Message--info">Please use the search on the top to list available assets.</div>
    @endif

    @if ($errors->any())
        @foreach ($errors->all() as $error)
            <div class="Message Message--error">{{ $error }}</div>
        @endforeach
    @endif
@endsection
