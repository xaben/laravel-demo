@extends('assets.details')

@section('media')
    <audio class="asset-media"
            controls
            src="{{ $asset->getMedia() }}">
        Your browser does not support the
        <code>audio</code> element.
    </audio>
@endsection
