@extends('assets.details')

@section('media')
    <video class="asset-media" controls>
        <source src="{{ $asset->getMedia() }}" type="video/mp4">
        Your browser does not support the video tag.
    </video>
@endsection
