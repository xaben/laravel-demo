@extends('assets.details')

@section('media')
    <img class="asset-media" src="{{ $asset->getMedia() }}"/>
@endsection
