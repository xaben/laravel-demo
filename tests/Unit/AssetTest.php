<?php

namespace Tests\Unit;

use App\Model\Asset;
use DateTimeImmutable;
use Tests\TestCase;

class AssetTest extends TestCase
{
    /**
     * @test
     */
    public function createFromData()
    {
        $data = json_decode(file_get_contents(__DIR__ . '/../samples/asset.json'), true);

        $asset = Asset::buildFromData($data);
        $this->assertEquals('Glenn Lecture With Crew of Apollo 11', $asset->getTitle());
        $this->assertEquals('image', $asset->getType());
        $this->assertEquals('200907190008HQ', $asset->getAssetId());
        $this->assertEquals('HQ', $asset->getCenter());
        $this->assertInstanceOf(DateTimeImmutable::class, $asset->getDateCreated());
        $this->assertEquals([
            'Apollo 11',
            'Apollo 40th Anniversary',
            'Buzz Aldrin',
            'National Air and Space Museum (NASM)',
            'Washington, DC',
        ], $asset->getKeywords());
        $this->assertEquals('https://images-assets.nasa.gov/image/200907190008HQ/200907190008HQ~thumb.jpg', $asset->getThumbnailUrl());
    }
}
