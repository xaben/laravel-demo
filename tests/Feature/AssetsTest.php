<?php

namespace Tests\Feature;

use Tests\TestCase;

class AssetsTest extends TestCase
{
    /**
     * @test
     */
    public function homepage(): void
    {
        $response = $this->get('/');

        $response->assertStatus(200);
        $response->assertSeeText('Please use the search on the top to list available assets.');
    }

    /**
     * @test
     */
    public function searchWithData(): void
    {
        $response = $this->get('/search?query=apollo+11&submit=submit&types[]=image&types[]=video&types[]=audio');

        $response->assertStatus(200);
    }

    /**
     * @test
     */
    public function searchNoData(): void
    {
        $response = $this->get('/search?query=qwerty&submit=submit&types[]=image&types[]=video&types[]=audio');

        $response->assertStatus(200);
        $response->assertSeeText('Sorry no items matched your search for "qwerty".');
    }

    /**
     * @test
     */
    public function details(): void
    {
        $response = $this->get('/assets/S69-25944');

        $response->assertStatus(200);
        $response->assertSeeText('Apollo 11 Geology training');
    }
}
