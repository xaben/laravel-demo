Nasa image and video library - Laravel demo
===========================================

### Possible improvements

- Add another caching repository - would wrap the request for a single asset (these details hardly ever change)
- Add more details to the details page (like metadata)
- Add some sample data to homepage instead of invitation to search
- Use HTML semantic tags instead of generic ones (<article> instead of <div>)
- Add SEO headers, and optimize other content regarding SEO
- Improve design and typography on details page (low contrast at the moment)
- Integrate custom video, audio players on details page
- Add download buttons to other image resolutions
- Add captions to video player
- Consider subclassing `Asset` class 
- Reformat all code with a single Code Style, add hooks and CI checks regarding that 
- Remove unneeded components from Bootstrap in order to minimize size
 
