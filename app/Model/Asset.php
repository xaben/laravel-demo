<?php

namespace App\Model;

use DateTimeImmutable;
use DomainException;
use InvalidArgumentException;

class Asset
{
    private const TYPE_AUDIO = 'audio';
    private const TYPE_IMAGE = 'image';
    private const TYPE_VIDEO = 'video';

    public const TYPES = [
        self::TYPE_AUDIO,
        self::TYPE_IMAGE,
        self::TYPE_VIDEO,
    ];

    private $assetId;

    private $type;

    private $thumbnailUrl;

    private $assets;

    private $data;

    private function __construct(string $assetId, string $type, array $data, ?string $thumbnailUrl, ?array $assets)
    {
        $this->assetId = $assetId;
        $this->type = $type;
        $this->thumbnailUrl = $thumbnailUrl;
        $this->data = $data;
        $this->assets = $assets;
    }

    public static function buildFromData(array $item): Asset
    {
        if (empty($item['data']) || !is_array($item['data'])) {
            throw new InvalidArgumentException('Cannot create AssetPreview: not enough data or bad data.');
        }

        return new self(
            $item['data'][0]['nasa_id'],
            $item['data'][0]['media_type'],
            $item['data'][0],
            $item['links'][0]['href'] ?? null,
            $item['assets'] ?? null
        );
    }

    public function getAssetId(): string
    {
        return $this->assetId;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getThumbnailUrl(): ?string
    {
        return $this->thumbnailUrl;
    }

    public function getTitle(): string
    {
        return $this->data['title'] ?? '';
    }

    public function getKeywords(): array
    {
        return $this->data['keywords'] ?? [];
    }

    public function getDateCreated(): ?DateTimeImmutable
    {
        if (!isset($this->data['date_created'])) {
            return null;
        }

        return new DateTimeImmutable($this->data['date_created']);
    }

    public function getCenter(): ?string
    {
        return $this->data['center'] ?? '';
    }

    public function getMedia(): ?string
    {
        if (empty($this->assets)) {
            throw new DomainException('No assets were found');
        }

        // todo: make this class abstract and move to subclasses
        switch ($this->getType()) {
            case Asset::TYPE_AUDIO:
                return $this->findAssetBySuffix('orig.wav');
            case Asset::TYPE_VIDEO:
                return $this->findAssetBySuffix('orig.mp4');
            case Asset::TYPE_IMAGE:
                return $this->findAssetBySuffix('large.jpg');
        }

        throw new DomainException('Bad type given');
    }

    protected function findAssetBySuffix(string $suffix): ?string
    {
        $suffixLength = strlen($suffix);
        foreach ($this->assets as ['href' => $asset]) {
            if (strlen($asset) < $suffixLength) {
                continue;
            }

            if (substr_compare($asset, $suffix, -$suffixLength, $suffixLength, true) === 0) {
                return $asset;
            }
        }

        return null;
    }
}
