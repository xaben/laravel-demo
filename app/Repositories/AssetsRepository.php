<?php

namespace App\Repositories;

use App\Model\Asset;

interface AssetsRepository
{
    public function findAll(string $query, array $types): array;

    public function getById(string $assetId): Asset;
}
