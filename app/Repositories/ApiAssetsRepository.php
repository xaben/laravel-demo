<?php

namespace App\Repositories;

use App\Model\Asset;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use InvalidArgumentException;

class ApiAssetsRepository implements AssetsRepository
{
    private $client;

    public function __construct()
    {
        // todo: inject
        $this->client = new Client([
            'base_uri' => 'https://images-api.nasa.gov',
        ]);
    }

    public function findAll(string $query, array $types, int $page = null): array
    {
        if (array_diff($types, ['image', 'video', 'audio'])) {
            throw new InvalidArgumentException('Bad media type.');
        }

        $response = $this->client->request('GET', '/search', [
            'query' => [
                'q' => $query,
                'media_type' => implode(',', $types),
                'page' => $page ?: 1,
            ],
        ]);
        $body = json_decode($response->getBody(), true);
        if (isset($body['collection']) && isset($body['collection']['items'])) {
            return array_map([Asset::class, 'buildFromData'], $body['collection']['items']);
        }

        return [];
    }

    public function getById(string $assetId): Asset
    {
        $assetInformation = $this->getMainAssetInformation($assetId);
        $assetInformation['assets'] = $this->getAssetUrls($assetId);

        return Asset::buildFromData($assetInformation);
    }

    private function getMainAssetInformation(string $assetId): array
    {
        try {
            $response = $this->client->request('GET', '/search', [
                'query' => [
                    'nasa_id' => $assetId,
                ],
            ]);
        } catch (GuzzleException $e) {
            throw new InvalidArgumentException('Cannot read asset information');
        }

        $mainData = json_decode($response->getBody(), true);
        if (
            empty($mainData['collection']['items']) ||
            count($mainData['collection']['items']) !== 1
        ) {
            throw new InvalidArgumentException('Bad asset information format');
        }

        return $mainData['collection']['items'][0];
    }

    private function getAssetUrls(string $assetId): array
    {
        try {
            $response = $this->client->request('GET', '/asset/' . $assetId);
        } catch (GuzzleException $e) {
            throw new InvalidArgumentException('Cannot read asset information');
        }
        $assetData = json_decode($response->getBody(), true);

        if (empty($assetData['collection']['items'])) {
            throw new InvalidArgumentException('Bad asset information format');
        }

        return $assetData['collection']['items'];
    }
}
