<?php

namespace App\Http\Controllers;

use App\Model\Asset;
use App\Repositories\ApiAssetsRepository;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use InvalidArgumentException;

class AssetsController extends Controller
{
    public function index()
    {
        return view('assets.search', ['types' => Asset::TYPES]);
    }

    public function search(Request $request)
    {
        $data = $request->validate([
            'query' => 'required',
            'types' => ['required', 'array'],
            'types.*' => Rule::in(Asset::TYPES),
        ]);

        ['query' => $query, 'types' => $types] = $data;

        return view('assets.search', [
            'items' => (new ApiAssetsRepository())->findAll($query, $types),
            'query' => $query,
            'types' => $types,
        ]);
    }

    public function show(string $assetId)
    {
        try {
            $asset = (new ApiAssetsRepository())->getById($assetId);
        } catch (InvalidArgumentException $e) {
            abort(404);
        }

        return view(
            'assets.details.' . $asset->getType(),
            [
                'asset' => $asset,
            ]
        );
    }
}
